---
title: "A propos"
layout: page
---


# Qui sommes-nous ?

Nous sommes une communauté informelle d'artisants logiciel. N'importe qui peut participer.


# Quel est l'intention de ce site ?

Ce site a pour but de faire un inventaire des techniques et pratiques que nous utilisons dans notre métier d'artisants logiciels.


# Quel choix de design du site ?

Le design de ce site est volontairement sobre, dans un style rétro geek. Voici les choix:

1. Tentative d'éco-conception. Minimiser l'impact environnemental du site.
2. Génération statique de sites, grâce à [hugo](https://gohugo.io). Cela limite la technologie à l’œuvre et cela a un effet positif sur le critère précédent.
3. Pas d'images d'illustration. Sur beaucoup de sites web, il y a pléthores d'images qui n'apportent rien au propos du texte. Cela prend de la place de stockage, de transfert et a donc un impact sur mon premier critère.
4. Pas de système de commentaires. Cela requiert d'utiliser soit d'utiliser un service tiers avec un risque d'espionnage des données, soit héberger un service de commentaires et donc un impact négatif sur l'impact environnemental.
5. Pas de mesure de trafic. Pas d'espionnage de données privées.
6. Utilisation d'images au format `svg`. Quand une image est utile, nous essayons de favoriser un format qui est léger et peut changer de taille au rendu.
