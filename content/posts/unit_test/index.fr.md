---
draft: true
title: "Tests unitaires"
author: "Olivier Albiez, Loïc Weissbart"
date: 2023-01-08
tags:
  - tests
  - muda
description: |
  Les tests unitaires
toc: true
---

Série sur les tests unitaires.


## Introduction


## Exemple

### Birthday Greetings kata

Voici un exemple tiré du [kata Birthday Greetings](https://codingdojo.org/kata/birthday-greetings/). Ici, nous avons utilisé une syntaxe proche de gherking pour représenter le cas de test et d'abstraire l'implementation.

{{< code language="java" title="Test du service BirthdayGreeter" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
public class BirthdayServiceUnitTest {
    private LocalDate today;
    private final List<Person> people = newArrayList();

    @Test
    public void we_should_wish_birthday_for_right_person() {
        given_today_is(2018, MARCH, 1);
        and_person("Alexa", born(2000, MARCH, 1));
        and_person("Alice", born(2004, AUGUST, 12));
        and_person("Isabelle", born(2004, MARCH, 1));
        the_people_we_wish_for_their_birthday_should_be("Alexa, Isabelle");
    }

    private void given_today_is(final int year, final Month month, final int day) {
        this.today = LocalDate.of(year, month, day);
    }

    private void and_person(final String name, final Birthdate birthdate) {
        people.add(Person.person(name(name)).with(birthdate));
    }

    private void the_people_we_wish_for_their_birthday_should_be(final String expected) {
        assertEquals(expected, personsToWishBirthday());
    }

    private String personsToWishBirthday() {
        final List<Person> selected = newArrayList();
        new BirthdayService(() -> people, selected::add, () -> today).process();
        return selected.stream().map(Person::name).map(Name::value).collect(Collectors.joining(", "));
    }

    private Birthdate born(final int year, final Month month, final int day) {
        return new Birthdate(year, month, day)
    }
}
{{< / code >}}


### Usage d'un DSL

Dans une application de génération de test basé sur les modèles, une fonctionalité demandée est de valider le modèle d'entrée pour y détécter des erreurs. Ici, nous voulons vérifier que deux opérations ne s'apellent pas mutuellement.

Le modèle d'entrée est écrit en UML avec de l'OCL[^1] en langage d'action. Pour l'écriture des tests, nous avons défini une représentation textuelle du modèle.

[^1]: Object Constraint Language

{{< code language="java" title="Validation de Model" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
    @Test
    public void cycleInRecursiveCallee() {
        assertEquals(
            """
            Operation.RecursiveCall beetween Name::callee1 and Name::callee2
            Operation.RecursiveCall beetween Name::callee2 and Name::callee1
            """,
            verify("""
                class Name:
                    callee1(): Boolean
                        post { result = callee2() }
                    callee2(): Boolean
                        post { result = callee1() }
                    caller(): Boolean
                        post { result = callee1() }
            """))
    }
{{< / code >}}


Dans une application qui doit réaliser des actions transactionelles, le test de l'execution des étapes de la transaction s'est fait avec un DSL pour cacher l'implémentation et exprimer l'intention plus clairement.

{{< code language="csharp" title="Transaction" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
    [Fact]
    public void executor_should_commit_all_transaction_when_no_errors()
    {
        Assert.Equal(
            "OK [S1:commit, S2:commit, S3:commit]",
            Transaction(Step("S1"), Step("S2"), Step("S3")));
    }

    [Fact]
    public void executor_should_rollback_already_commited_transaction_when_error()
    {
        Assert.Equal(
            "KO:PAF [S1:commit]",
            Transaction(
                FailedStep("S1", error:"PAF"),
                Step("S2"),
                Step("S3")));

        Assert.Equal(
            "KO:PAF [S1:commit, S2:commit, S1:rollback]",
            Transaction(
                Step("S1"),
                FailedStep("S2", error: "PAF"),
                Step("S3")));

        Assert.Equal(
            "KO:PAF: [S1:commit, T2:commit, T3:commit, T2:rollback, S1:rollback]",
            Transaction(
                Step("S1"),
                Step("S2"),
                FailedStep("S3", error: "PAF")));
    }
{{< / code >}}




### Usage d'un ascii art

Dans le cadre du jeu de la vie, voici un test qui verifie l'evolution d'une grille avec un *glider*. Le choix d'en faire une représentation ascii art permet d'avoir immédiatement une compréhension de ce que l'on veut vérifier.

{{< code language="java" title="Game of life" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}

    @Test
    public void testGliderScenario() {
        assertEvolve(4,
                "  ......  ......  ......  ......  ......  ",
                "  ..#...  ......  ......  ......  ......  ",
                "  ...#..  .#.#..  ...#..  ..#...  ...#..  ",
                "  .###..  ..##..  .#.#..  ...##.  ....#.  ",
                "  ......  ..#...  ..##..  ..##..  ..###.  ",
                "  ......  ......  ......  ......  ......  ");
    }
{{< / code >}}

Dans une implémentation d'une aide pour le jeu *Capitain Sonar* – dans lequel on veut aider à localiser le sous-marin ennemi – nous recevons la liste des déplacements du sous-marin et nous devons regarder ses positions possibles qui respectent les règles de mouvements.

Le choix s'est porté sur des tests ascii art pour avoir une représentation compréhensible.

{{< code language="python" title="Capitain Sonar" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}

# ◎ possible opponent position
# • sea
# ▲ island
def test_tracking_with_multiples_moves_constraints():
    scenario(
        """
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ • • • • • ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ ▲ ▲ • • • ┃ • • • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ • • • • ▲ ┃ ▲ ▲ • • • ┃
            ┃ • • • • • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ • ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
        """,
        constraint(CanMove.to(Direction.North)),
        constraint(CanMove.to(Direction.West)),
        constraint(CanMove.to(Direction.West)),
        expect("""
            ┏━━━━━━━━━━━┳━━━━━━━━━━━┓
            ┃ ◎ ◎ ◎ ◎ ◎ ┃ • • ▲ ▲ • ┃
            ┃ ▲ ▲ ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ ▲ ▲ • • • ┃ ◎ ◎ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┣━━━━━━━━━━━╋━━━━━━━━━━━┫
            ┃ ◎ ◎ • • ▲ ┃ ▲ ▲ ◎ • • ┃
            ┃ • • ◎ ◎ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ◎ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ ◎ ◎ ◎ • • ┃
            ┃ • ▲ ▲ ▲ ▲ ┃ • • • • • ┃
            ┗━━━━━━━━━━━┻━━━━━━━━━━━┛
            """))

{{< / code >}}

### DSL et génération aléatoire

Dans un jeu de cartes comprenant des phases de paris, un joueur peut souhaiter faire monter la mise. Ce test initialise un jeu à une des phases appropriées, et envoie une commande de montée de paris qui est valide, bien qu'avec une valeur tirée au hasard.

{{< code language="kotlin" title="Sabacc" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}

@Test
fun `After a player has raised to X credits, she has X less credits and the main pot has X more credits`() {
    given {
        phase(AnyOf("Betting", "Calling"))
        currentPlayer(canOvercomeBet)
    }.on {
        raiseBet("current player", between(betLevel, ownedCredits))
    }.then {
        mainPotGained(raiseAmount)
        player("current player").lostCredits(raiseAmount)
    }
}
{{< / code >}}

Dans le même jeu, durant une phase de distribution de carte, les joueurs peuvent choisir de défausser une de leurs cartes et de piocher la première carte du jeu. Le DSL cache toute la mise en place du deck et de la main du joueur.

{{< code language="kotlin" title="Sabacc" id="2" expand="Show" collapse="Hide" isCollapsed="false" >}}

@Test
fun `If player trades a card, her hand should have one new card and lost the traded card`() {
    given {
        phase("Dealing")
        currentPlayer(withHandSize(anyHandSize))
    }.on {
        trade("current player", "any card")
    }.then {
        player("current player")
                .handDoesNotContains("targeted card")
                .handContains("first deck card")
    }
}

{{< / code >}}

Dans un jeu de conquête spatiale, les peuples galactiques possèdent des systèmes stellaires en fonction desquels des règles sont appliquées. Dans l'exemple suivant (calcul des capacités scientifique d'un peuple), toutes les données nécessaires à l'existence d'un peuple mais non nécessaire au calcul de la science sont générées aléatoirement. Le test est ainsi limité aux données seules dont il a besoin, ce qui rend l'extraction future du contexte "recherche" plus simple.

{{< code language="kotlin" title="Space Pangee" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}

@Test
internal fun `If people have enough research point, then their research level should increase by one point`() {
    given {
        species("Scerck Lewer") {
            research {
                development = 4
                propulsion = 2
                weaponry = 2
            }
        }
        `star system`("Ho-gan") {
            type = 4
            development = 4
            owner = "Scerck Lewer"
        }
        `star system`("Streng") {
            type = 2
            development = 1
            owner = "Scerck Lewer"
        }
        `star system`("Sieng") {
            type = 3
            development = 3
            owner = "Scerck Lewer"
        }
    }.`when species`("Scerck Lewer").`tries to research`("propulsion") {
        `their research levels should be`(
            development = 4,
            propulsion = 3,
            weaponry = 2
        )
    }
}

{{< / code >}}

### Encore un peu d'ASCII art

Pour détailler une carte galactique aux données complexes (un système stellaire peut avoir un niveau de développement, un propriétaire, un nom, etc), on utilise un opérateur spécifique créé pour l'occasion, "string.x(string)", qui va combiner des dessins entre eux pour obtenir les données nécessaires.

{{< code language="kotlin" title="Space Pangee" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}

    @Test
    internal fun `Internal trade`() {
        given {
            agreements {
                trades(
                    "bbb" to "aaa"
                )
            }
            galaxies {
                """
                    any   CCC   any
                        \  |  /
                          AAA
                        /
                    any - any
                """ x ("""
                    [Development]
                    014   002   026
                        \  |  /
                          012
                        /
                    011 - 004
                """ x """
                    [Owners]
                    aaa   bbb   ccc
                        \  |  /
                          bbb
                        /
                    aaa - bbb
                """)
            }.`system meets expectations`("AAA") {
                internalTradeIncomes = 12 * 1
            }
        }
    }

{{< / code >}}


### Personas et jeu de données de test

Les personas[^persona] créées par les *UX designers* et *Product Owners* sont une source pour créér un jeu de données de test cohérent : ils sont partagés , a minima, au sein de l'équipe, cohérents et font référence à toute information utile pour l'application

Ils permettent, en outre, de créer des tests à partir d'exemples que l'équipe peut co-construire en faisant référence à des informations standards connus de tous.

Le test suivant se base sur les personas du chirurgien *Dr Emily* et le patient *Grace* de façon à exprimer au mieux la règle métier et de simplifier sa lecture.

{{< code language="kotlin" title="Space Pangee" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
    @Test
    internal fun `Onboarding a new patient should welcome them`() {
        given {
            `a new patient named`(GRACE)
        }
        `when` {
            Dr_EMILY.onboard(GRACE)
        } then {
            GRACE.`should be registered as patient`()
            GRACE.`should be welcomed to fill their profile`()
        }
    }

{{< / code >}}

Utiliser les personas dans les tests peut renforcer dans l'esprit de l'équipe la raison d'être du code, du pour qui et pourquoi nous développons.

[^persona]: Portrait-robot, d'un utilisateur type de l'application permettant de se mettre dans la peau de ce dernier.