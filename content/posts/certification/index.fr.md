---
draft: true
title: "Les certifications"
author: "Olivier Albiez et Romeu Moura"
date: 2023-01-16
tags:
  - philosophie
description: |
  Discours sur les certifications.
review: structure
---


1. Ce que le marché attends d'une certification
   1. Être un tier de confiance pour la séléction des candidats
   2. Avoir des critères objectifs de certification
   3. Avoir une grande quantité de personnes certifiées (grosse demande dans certain domaines, comme scrum master)
2. Le modèle économique de la certification (conflit d'interet)
3. Le paradox de la certification. Pour les personnes certifiées, cela ouvre des portes. La certification devient un but en soi, à la fois pour avoir du taf et comme barière d'entrée pour les autres. On risque l'effet "Yoda" : que la certification deviennent une caricature d'elle-même.


Une certification qui pourrait marcher :
1. Peu de personnes certifiées
2. Excellente qualité et pas de critère objectif, plus une évaluation semi-subjective des pairs.
3. Assurance sur la qualité (soit dans le style d'un conseil de l'ordre, soit dans le principe d'assurance sur défaillance)

