---
draft: true
title: "Introduction à SOLID"
author: Olivier Albiez
date: 2021-06-23
lang: fr
tags:
  - conception
description: |
  SOLID regroupe cinq principes de conception utiles en programmation objet.
---

Ces principes ont été introduit par Michael Feathers et Robert C. Martin au début des année 2000.


> SOLID is a mnemonic acronym for five design principles intended to make software designs *more understandable*, *flexible* and *maintainable*. [wikipedia](https://fr.wikipedia.org/wiki/SOLID_(informatique))




## Single responsibility principle (Responsabilité unique)

An artifact should have one and only one reason to change, meaning that an artefact should have only one job.


{{< highlight java >}}
public String getThreatLevelFor(Aircraft aircraft) {
    GeographicalRepository database = GeographicalRepositoryProvider.getInstance();

    if (database == null) {
      return "";
    } else if (database instanceof SQLGeographicalDatabase) {
      try {
        ((SQLGeographicalDatabase) database).connect();
      } catch (DBConnectionException exception) {
        System.out.println("Cannot get areas: access to DB refused");
        return "";
      }
    }

    Vector3D ownLocation = database.getOwnLocation();
    List<Area> areasInSector = new ArrayList<>();
    for (Area area : database.getAllAreas()) {
      double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2)
                      + Math.pow(area.center.y - ownLocation.y, 2));
      if (distance <= 1000) {
        areasInSector.add(area);
      }
    }

    return process(aircraft, areasInSector);
}
{{< / highlight >}}


Smells:
- Large Class
- Long Method
- Lot of methods
- High Coupling/Low cohesion
- Helper class
- Multiple functional/technical concepts at the same



## Open/closed principle (Ouvert/fermé)

Objects or entities should be open for extension, but closed for modification.


{{< highlight java >}}
private static String process(Aircraft aircraft, List<Area> areas) {
    double norm = Math.sqrt(Math.pow(aircraft.speed.x, 2)
        + Math.pow(aircraft.speed.y, 2)
        + Math.pow(aircraft.speed.z, 2));

    if (norm > 2000)
      return "HIGH";
    if (aircraft.type.equals("MIG"))
      return "HIGH";
    if (aircraft.type.equals("Rafale"))
      return "LOW";
    if (aircraft.type.equals("A-320")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "LOW";
    }

    if (aircraft.type.equals("F-35") || aircraft.type.equals("UNKNOWN")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "MEDIUM";
    }
    return "MEDIUM";
  }
{{< / highlight >}}


Smells:
- Complex switch/Lot of ifs
- High cyclomatic complexity


## Liskov substitution principle (Substitution de Liskov)

Every subclass/derived class should be substitutable for their base/parent class.

{{< highlight java >}}
public String getThreatLevelFor(Aircraft aircraft) {
  GeographicalRepository database = GeographicalRepositoryProvider.getInstance();

  if (database == null) {
    return "";
  } else if (database instanceof SQLGeographicalDatabase) {
    try {
      ((SQLGeographicalDatabase) database).connect();
    } catch (DBConnectionException exception) {
      System.out.println("Cannot get areas: access to DB refused");
      return "";
    }
  }

  Vector3D ownLocation = database.getOwnLocation();
  List<Area> areasInSector = new ArrayList<>();
  for (Area area : database.getAllAreas()) {
    double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2)
                    + Math.pow(area.center.y - ownLocation.y, 2));
    if (distance <= 1000) {
      areasInSector.add(area);
    }
  }

  return process(aircraft, areasInSector);
}
{{< / highlight >}}

Smells:
- You have to check for the type provided (e.g. instanceof)
- You use null value (null, nil, None, ...)


## Interface segregation principle (Ségrégation des interfaces)

A client should never be forced to implement an interface that it doesn’t use or clients shouldn’t be forced to depend on methods they do not use.

{{< highlight java >}}
interface GeographicalRepository {
  List<Area> getAllAreas();

  Vector3D getOwnLocation();

  LocalTime getReferenceTime();
}
{{< / highlight >}}

Smells:
- Fat interface/Class with lot of methods
- Interface has multiple responsibilities
- Difficulties to expose a subset of responsibilities


## Dependency inversion principle (Inversion des dépendances)

Entities must depend on abstractions not on concretions. It states that the high level module must not depend on the low level module, but they should depend on abstractions.

{{< highlight java >}}
public String getThreatLevelFor(Aircraft aircraft) {
  GeographicalRepository database = GeographicalRepositoryProvider.getInstance();

  if (database == null) {
    return "";
  } else if (database instanceof SQLGeographicalDatabase) {
    try {
      ((SQLGeographicalDatabase) database).connect();
    } catch (DBConnectionException exception) {
      System.out.println("Cannot get areas: access to DB refused");
      return "";
    }
  }

  Vector3D ownLocation = database.getOwnLocation();
  List<Area> areasInSector = new ArrayList<>();
  for (Area area : database.getAllAreas()) {
    double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2)
                    + Math.pow(area.center.y - ownLocation.y, 2));
    if (distance <= 1000) {
      areasInSector.add(area);
    }
  }

  return process(aircraft, areasInSector);
}
{{< / highlight >}}

Smells:
- Dependencies between classes (vs interface)
- Monolithic architecture
- Abstraction depends on details/implementation
