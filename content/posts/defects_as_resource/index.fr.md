---
title: "Les défauts comme source d'apprentissage"
author: "Olivier Albiez"
date: 2021-07-05
tags:
  - kaizen
  - muda
description: |
  Comment utiliser les défauts comme des ressources.
---


Dans la vie d'une équipe de développement, il est fréquent que l'on rencontre un défaut. Il peut s'agir d'un bug qui sort du processus de développement[^1] ou pire qui arrive en production. Il peut aussi résulter d'une incompréhension entre l'expression du besoin et de la fonctionnalité développée. _Plus généralement, il s'agit d'un décalage entre ce que l'on voulait et ce que l'on obtient[^2]._

Beaucoup d'équipes se limitent à corriger superficiellement les défauts rencontrés et perdent de formidables opportunités d'apprentissage. Pour les exploiter au maximum, posons-nous 3 questions :

1. Que dit ce défaut de notre stratégie de test ?
2. Que dit ce défaut de nos pratiques techniques ?
3. Que dit ce défaut de nos pratiques de collaboration ?

La première question permet de comprendre pourquoi le bug est sorti du processus de développement. Une bonne pratique et de tenter de reproduire le bug avec un test automatisé avant de le corriger. Mais il nous faut encore creuser plus loin ; est-ce que nous n'aurions pas oublié une typologie de cas de tests ? Par exemple, un défaut lors d'un déploiement nous a appris que nous ne testions pas les fichiers de configuration.

La deuxième question permet de regarder s'il n'y a pas une vulnérabilité dans nos techniques de programmation. Toutes les manières d'écrire du code ne sont pas équivalentes. Il est difficile, surtout pour les débutants, de percevoir les vulnérabilités dans la manière d'écrire. Heureusement, les défauts permettent de les déceler. Par exemple, dans un contexte de programmation multithreadé, nous avons résolu une avalanche de bugs de synchronisation en utilisant des objets immuables.

La troisième question permet d'interroger le fonctionnement de l'équipe. Comment notre équipe recueille le besoin, se forme, se synchronise, s’entraide, contrôle la qualité de notre production... Il est tentant de chercher le responsable qui a introduit le défaut, il ne faut pas oublier que le reste de l'équipe a laissé faire. Par exemple, une équipe, qui a laissé un développeur junior réaliser une fonctionnalité délicate, a décidé de faire toutes les fonctionnalités complexes en binôme.

Pour répondre à ces questions, nous avons organisé des rétrospectives régulières dédié à nos défauts. Cela nous a permis de trouver des solutions pour éliminer définitivement les causes de plein de défauts. De cette manière, ces défauts sont devenus des ressources pour l'amélioration de notre équipe.


[^1]: Pour moi, le processus de développement est terminé quand l'équipe dit que la fonctionnalité est finie. Il peut encore y avoir des étapes avant de partir en production.

[^2]: Je fais intentionnellement une définition restrictive d'un défaut. Dans le Lean, le terme utilisé est le gaspillage et est plus général.
