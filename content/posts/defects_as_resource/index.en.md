---
title: "Defects as learning resources"
author: Olivier Albiez
date: 2021-07-05
tags:
  - kaizen
  - muda
description: |
  How to use defects to improve your team.
---

In the life of a development team, it is common to encounter a defect. It may be a bug that leaves the development process[^1] or worse that arrives in production. It may also result from a misunderstanding between the expression of the need and the functionality developed. _More generally, it is a mismatch between what you wanted and what you got[^2]._

Many teams limit themselves to correct the defects encountered and lose out on great learning opportunities. To make the most of them, let's ask ourselves 3 questions:

1. What does this defect say about our testing strategy?
2. What does this defect say about our technical practices?
3. What does this defect say about our collaboration practices?

The first question helps to understand why the bug went out of the development process. A good practice is to try to reproduce the bug with an automated test before fixing it. But we need to dig deeper; have we not forgotten a type of test case? For example, a defect during a deployment taught us that we were not testing the configuration files.

The second question is whether there is a vulnerability in our programming techniques. Not all ways of writing code are equal. It is difficult, especially for beginners, to see the vulnerabilities in the way we write. Fortunately, flaws can be detected. For example, in a multithreaded programming context, we solved an avalanche of synchronisation bugs by using immutable objects.

The third question allows us to question the functioning of the team. How does our team gather the need, train, synchronise, help each other, control the quality of our production... It is tempting to look for the person responsible for introducing the defect, but we must not forget that the rest of the team let it happen. For example, a team that let a junior developer do a tricky feature decided to do all the complex features in pairs.

To address these issues, we organised regular retrospectives dedicated to our defects. This allowed us to find solutions to permanently eliminate the causes of many defects. In this way, these defects became resources for the improvement of our team.


[^1]: For me, the development process is finished when the team says the feature is finished. There may still be steps before it goes into production.

[^2]: I am intentionally making a restrictive definition of a defect. In Lean, the term used is waste and is more general.
