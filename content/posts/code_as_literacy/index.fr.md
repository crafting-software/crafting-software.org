---
draft: true
title: "Coder est de la littérature"
author: "Olivier Albiez"
date: 2021-07-14
tags:
  - philosophie
description: |
  Discours sur la nature du code.
review: structure
---

{{< review level="structure" >}}


C'est quoi coder ? Une réponse évidente est de formaliser un comportement que l'on souhaite faire réaliser par un ordinateur. Cette réponse n'est pas suffisante, car elle n'explique pas assez l'évolution des langages de programmation et des pratiques de développements.

En effet, formaliser un comportement pourrait se faire de cette manière :

{{< code language="llvm" title="Code assembleur" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
; Exemple prit sur https://fr.wikipedia.org/wiki/Assembleur

str:
  .ascii "Bonjour\n"
  .global _start

_start:
  movl $4, %eax
  movl $1, %ebx
  movl $str, %ecx
  movl $8, %edx
  int $0x80
  movl $1, %eax
  movl $0, %ebx
  int $0x80
{{< / code >}}

Ce code permet d'afficher "Bonjour". Il fonctionne, mais n'est pas agréable ni à __lire__ ni à __comprendre__.

Du coup, coder, est aussi une communication à destination d'un être humain ; soit d'une autre personne, soit de soi-même dans le futur. Ce que l'on essaye de communiquer, ce sont deux choses :

1. Que fait le programme, à quoi sert-il ?
2. Comment le programme réalise-t-il le comportement ?

Habituellement, nous répondons à ces deux questions dans des parties différentes de la base de code. Les tests permettent de répondre à la première question et le reste du code à la deuxième question.

Dire que coder est de la littérature est volontairement caricaturale, c'est pour faire prendre conscience que l'action de coder doit être pensée comme une action de communication à d'autres êtres humains. Donc il faut s'inspirer des meilleurs techniques que la littérature nous offre pour le faire et ne pas se contenter d'avoir un code qui marche.

Voici les conséquences que nous pouvons en tirer :

1. Il est probable que le premier jet d'écriture soit médiocre et qu'il faille le travailler. En particulier, il y a une différence entre "être facile à écrire" et "être facile à lire". La stratégie que nous avons adopté est de commencer par un code facile à écrire, puis de le transformer en code facile à lire. C'est une technique de séparation de l'attention[^1].

2. Seule une relecture du code permettra de savoir si le code est lisible et compréhensible. L'auteur à souvent du mal à être un bon juge de sa prose.



[^1]: En informatique il y a un adage qui explicite cela : "make it work, make it right, make it fast".
