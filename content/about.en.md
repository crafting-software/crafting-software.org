---
title: "About"
layout: page
---

# Who are we?

We are an informal community of software artisans. Anyone can participate.


# What is the purpose of this site?

The purpose of this site is to make an inventory of the techniques and practices we use in our work as software artisans.


# What is the design of the site?

The design of this site is deliberately sober, in a retro geek style. Here are the choices:

1. Attempting to be eco-friendly. Minimise the environmental impact of the site.
2. Static site generation, using [hugo](https://gohugo.io). This limits the technology at work and has a positive effect on the previous criterion.
3. No illustrative images. On many websites, there is a plethora of images that do not add anything to the text. This takes up storage space, transfer space and therefore has an impact on my first criterion.
4. No comment system. This requires either using a third party service with a risk of data spying, or hosting a comment service and therefore a negative impact on the environmental impact.
5. No traffic measurement. No spying on private data.
6. Use of images in `svg` format. When an image is useful, we try to use format that is lightweight and can change size when rendered.
